#[derive(Debug, Clone)]
pub struct PomTimer {
    minutes: usize,
    seconds: usize,
}

impl std::fmt::Display for PomTimer {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:02}:{:02}", self.minutes, self.seconds)
    }
}

impl PomTimer {
    pub fn new() -> PomTimer {
        PomTimer {
            minutes: 0,
            seconds: 0,
        }
    }
    pub fn set_time_ms(&mut self, ms: usize) {
        self.seconds = (ms / 1000) % 60;
        self.minutes = (ms / 1000) / 60;
        //println!("{}mins ",self.minutes);
    }

    pub fn set_time_minutes(&mut self, minutes: usize) {
        self.minutes = minutes;
        self.seconds = 0;
    }

    pub fn dec_sec(&mut self) {
        let ms_time = self.ms_from_time();
        self.set_time_ms(ms_time - 1000);
    }

    pub fn ms_from_time(&mut self) -> usize {
        self.minutes * 60000 + self.seconds * 1000
    }
    //pub fn get_time(&self) -> String {

    //}
}

#[cfg(test)]
mod tests{
    use super::*;
    #[test]
    fn test_set_mstimer(){
        let mut pt = PomTimer::new();
        pt.set_time_ms(3600);
        println!("m{:?} s{:?}",pt.minutes,pt.seconds);
        assert_eq!(pt.minutes,0);
        assert_eq!(pt.seconds,3);

        pt.set_time_ms(59*1000);
        assert_eq!(pt.minutes,0);
        assert_eq!(pt.seconds,59);

        pt.set_time_ms(5*1000*60);
        assert_eq!(pt.minutes,5);
        assert_eq!(pt.seconds,0);
    }

    #[test]
    fn test_set_time_minutes(){
        let mut pt = PomTimer::new();
        pt.set_time_minutes(4);
        assert_eq!(pt.minutes,4);
    }

    #[test]
    fn test_dec_sec(){
        let mut pt = PomTimer::new();
        pt.set_time_minutes(1);
        pt.dec_sec();
        assert_eq!(pt.minutes,0);
        assert_eq!(pt.seconds,59);
    }
    #[test]
    fn test_ms_from_time(){
        let mut pt = PomTimer::new();
        pt.set_time_ms(3000);
        assert_eq!(pt.ms_from_time(),3000);
    }
}
