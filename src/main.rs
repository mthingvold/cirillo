mod pomstate;
mod pomtimer;
mod view;
use std::time::{Duration, Instant};
//use std::thread;
use pomstate::PomodoroState;
use pomtimer::PomTimer;
use std::thread::sleep;

fn main() {
    let mut tomato = PomTimer::new();
    let mut state = PomodoroState::new(tomato);
    state.start_session();
    //thread::spawn(move || {view::curses_menu()});
    //sleep(Duration::from_millis(1000 - sync_offset));
}
