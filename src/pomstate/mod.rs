use crate::pomtimer::*;
use std::thread::sleep;
use std::time::{Duration, Instant};

const DEFAULT_VEC: [State; 8] = [
    State::INACTIVE,
    State::WORK,
    State::ShortBreak,
    State::WORK,
    State::LongBreak,
    State::WORK,
    State::ShortBreak,
    State::WORK,
];
#[derive(Debug, Clone, Copy)]
enum State {
    INACTIVE,
    WORK,
    ShortBreak,
    LongBreak,
}
#[derive(Debug, Clone)]
pub struct PomodoroState {
    state_vec: Vec<State>,
    index: usize,
    work_time: usize,
    rest_time: usize,
    timer: PomTimer,
    timer_start: Option<Instant>,
}

impl PomodoroState {
    pub fn new(timer: PomTimer) -> PomodoroState {
        PomodoroState {
            state_vec: DEFAULT_VEC.to_vec(),
            index: 0,
            work_time: 1500000,
            rest_time: 300000,
            timer,
            timer_start: None,
        }
    }

    pub fn next_state(&mut self) {
        self.index += 1;
    }

    pub fn get_state(&self) -> State {
        self.state_vec[self.index]
    }
    pub fn get_state_string(&self) -> &str {
        match self.get_state() {
            State::INACTIVE => "Not Running",
            State::WORK => "Work Time!",
            State::ShortBreak => "Take a quick break!",
            State::LongBreak => "Time for a longer rest",
        }
    }

    pub fn start_timer(&mut self) {
        self.timer_start = Some(Instant::now());
        loop {
            let instant = self.timer_start.unwrap().elapsed().as_millis() as usize;
            let time_passed = self.work_time - self.timer.ms_from_time();

            let offset = (instant - time_passed) as u64;

            sleep(Duration::from_millis(1000 - offset));

            self.timer.dec_sec();
            print!("{esc}[2J{esc}[1;1H", esc = 27 as char);
            println!("{}", self.get_state_string());
            println!("{}", self.timer);

            if self.timer.ms_from_time() == 0 {
                break;
            }
        }
    }
    pub fn start_session(&mut self) {
        self.index = 1;
        while self.index < self.state_vec.len() {
            match self.state_vec[self.index] {
                State::INACTIVE => break,
                State::WORK => self.start_work(),
                State::ShortBreak => self.start_short_break(),
                State::LongBreak => self.start_long_break(),
            };
            self.index += 1;
        }
    }

    pub fn start_work(&mut self) {
        self.timer.set_time_ms(self.work_time);
        self.start_timer();
    }

    fn start_short_break(&mut self) {
        self.timer.set_time_ms(self.rest_time);
        self.start_timer();
    }

    fn start_long_break(&mut self) {
        self.timer.set_time_ms(self.rest_time * 2);
        self.start_timer();
    }
}
