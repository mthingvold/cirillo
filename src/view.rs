extern crate ncurses;
use ncurses::*;
use std::process::exit;
const KEY_Q_LOWER: i32 = 113;
const KEY_Q: i32 = 81;
///Setup an Ncurses instance is responsible for
///input management and display
//TODO add a reference to the time struct and show it.
pub fn curses_menu() {
    //setup ncurses
    initscr();
    raw();

    //Allow extended keys, i.e F1
    keypad(stdscr(), true);
    noecho();

    addstr("Character Test: ");

    loop {
        //wait for input
        let c = getch();
        match c {
            KEY_F1 => {
                attron(A_BOLD());
                addstr("Help Menu Example");
                attroff(A_BOLD())
            }
            KEY_Q_LOWER | KEY_Q => {
                endwin();
                exit(0)
            }
            _ => addstr("Other key pressed"),
        };
        refresh();
    }
}
